const mathFunctions = require('./mathFunctions');

// .toBe
test('Expect 2 + 2 to be 4', () => {
    expect(mathFunctions.add(2,2)).toBe(4);
});

// Or not.toBe
test('Expect 2 + 2 not to equal 5', () => {
    expect(mathFunctions.add(2,2)).not.toBe(5);
});

// Skip a test with the x prefix
xtest('This test will be skipped', () => {
    expect(mathFunctions.subtract(5,3)).toBe(2);
});

// Or skip with the skip() function
test.skip('This test will be skipped', () => {
    expect(mathFunctions.subtract(6,3)).toBe(3);
});

// Parameterized Tests
test.each([[1,1,2], [2,2,4], [2,1,3]])(
    '%i + %i equals %i', (num1, num2, expected) => {
        expect(mathFunctions.add(num1, num2)).toBe(expected);
    }
)